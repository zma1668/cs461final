﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs461final
{

    class BaseController:Controller, EventListener
    {
        EventListener l;
        
        public BaseController(EventListener ls) :base()
        {
            l = ls;
        }
        public void ProcessEvent() { }

    }
}
